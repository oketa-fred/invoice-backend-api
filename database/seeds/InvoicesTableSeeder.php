<?php

use App\Customer;
use App\Invoice;
use App\Product;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Illuminate\Support\Facades\DB;

class InvoicesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();

        foreach (range(1, 50) as $index)  {
            $invoice = Invoice::create([
                "date"          => $faker->date("Y-m-d"),
                "customer_id"   => $index,
                "invoice_no"    => time() + $index,
            ]);

            // Insert Test Data into invoice_products table
            DB::table('invoice_products')->insert([
                'invoice_no' => $invoice->invoice_no,
                'product_id' => $index
            ]);
        }

    }
}

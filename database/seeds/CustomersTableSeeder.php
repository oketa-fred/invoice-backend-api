<?php

use App\Customer;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Illuminate\Support\Facades\DB;

class CustomersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();

        foreach (range(1, 100) as $index)  {
            Customer::create([
                "first_name"    => $faker->firstName,
                "last_name"     => $faker->lastName,
                "email"         => $faker->unique()->safeEmail,
                "phone"         => $faker->unique()->phoneNumber,
                "address"       => $faker->address,
            ]);
        }
    }
}

<?php

use App\Product;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Illuminate\Support\Facades\DB;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();

        foreach (range(1, 100) as $index)  {
            Product::create([
                "name"          => $faker->sentence(3),
                "price"         => $faker->numberBetween(50, 20000),
                "description"   => $faker->sentence,
                "service"       => $faker->boolean
            ]);
        }
    }
}

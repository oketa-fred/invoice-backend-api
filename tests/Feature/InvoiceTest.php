<?php

namespace Tests\Feature;

use App\Customer;
use App\Invoice;
use App\Product;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class InvoiceTest extends TestCase
{
    use RefreshDatabase;
    /**
     * Test can create an invoice
     */
    public function test_can_create_an_invoice()
    {
        $customer = Customer::create([
            "first_name" => "Jane",
            "last_name" => "Doe",
            "email" => "janedoe@gmail.com",
            "phone" => "256787584128",
            "address" => "kampala Kireka"
        ]);

        Product::create([
            "name" => "product name test 321",
            "price" => 7000,
            "description" => "Product description test",
            "service" => true
        ]);

        $data = [
            "date"          => date("Y-m-d"),
            "products"      => Product::pluck('id')->toArray(),
            "invoice_no"    => time()
        ];

        $response = $this->post(route('invoices.store', $customer->id), $data);

        $response->assertStatus(200);
    }

    /**
     * Test can list all invoices
     */
    public function test_can_list_invoices()
    {
        $customer = Customer::create([
            "first_name" => "Jane",
            "last_name" => "Doe",
            "email" => "janedoe@gmail.com",
            "phone" => "256787584128",
            "address" => "kampala Kireka"
        ]);

        Product::create([
            "name" => "product name test 321",
            "price" => 7000,
            "description" => "Product description test",
            "service" => true
        ]);

        $data = [
            "date"          => date("Y-m-d"),
            "customer_id"   => $customer->id,
            "products"      => Product::pluck('id')->toArray(),
            "invoice_no"    => time()
        ];

        Invoice::create($data);
        $response = $this->get(route('invoices.index', $customer->id));
        $response->assertStatus(200);
    }

    /**
     * test can show an invoice
     */
    public function test_can_show_an_invoice()
    {
        $customer = Customer::create([
            "first_name" => "Jane",
            "last_name" => "Doe",
            "email" => "janedoe@gmail.com",
            "phone" => "256787584128",
            "address" => "kampala Kireka"
        ]);

        Product::create([
            "name" => "product name test 321",
            "price" => 7000,
            "description" => "Product description test",
            "service" => true
        ]);

        $data = [
            "date"          => date("Y-m-d"),
            "customer_id"   => $customer->id,
            "products"      => Product::pluck('id')->toArray(),
            "invoice_no"    => time()
        ];

        $invoice = Invoice::create($data);
        $response = $this->get(route('invoices.show', [$customer->id, $invoice->invoice_no]));
        $response->assertStatus(200);
    }

    /**
     * Test can update an invoice
     */
    public function test_can_update_an_invoice()
    {
        $customer = Customer::create([
            "first_name" => "Jane",
            "last_name" => "Doe",
            "email" => "janedoe@gmail.com",
            "phone" => "256787584128",
            "address" => "kampala Kireka"
        ]);

        Product::create([
            "name" => "product name test 321",
            "price" => 7000,
            "description" => "Product description test",
            "service" => true
        ]);

        $invoice = Invoice::create([
            "date"          => date("Y-m-d"),
            "customer_id"   => $customer->id,
            "products"      => Product::pluck('id')->toArray(),
            "invoice_no"    => time()
        ]);

        Product::create([
            "name" => "product name test 321 new",
            "price" => 49000,
            "description" => "Product description test new",
            "service" => true
        ]);
            
        $data = [
            "date"          => date("Y-m-d"),
            "customer_id"   => $customer->id,
            "products"      => Product::pluck('id')->toArray(),
            "invoice_no"    => time()
        ];

        $response = $this->patch(route('invoices.update', [$customer->id, $invoice->invoice_no]), $data);
        $response->assertStatus(202);
    }

    /**
     * Test can delete an invoice
     */
    public function test_can_delete_an_invoice()
    {
        $customer = Customer::create([
            "first_name" => "Jane",
            "last_name" => "Doe",
            "email" => "janedoe@gmail.com",
            "phone" => "256787584128",
            "address" => "kampala Kireka"
        ]);

        Product::create([
            "name" => "product name test 321",
            "price" => 7000,
            "description" => "Product description test",
            "service" => true
        ]);

        $invoice = Invoice::create([
            "date"          => date("Y-m-d"),
            "customer_id"   => $customer->id,
            "products"      => Product::pluck('id')->toArray(),
            "invoice_no"    => time()
        ]);

        $response = $this->delete(route('invoices.destroy', [$customer->id, $invoice->invoice_no]));
        $response->assertStatus(204);
    }
}

<?php

namespace Tests\Feature;

use App\Customer;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CustomerTest extends TestCase
{
    use RefreshDatabase;
    
    /**
     * Test creation of a customer
     */
    public function test_can_create_a_customer()
    {
        $data = [
            "first_name" => "Jane",
            "last_name" => "Doe",
            "email" => "janedoe@gmail.com",
            "phone" => "256787584128",
            "address" => "kampala Kireka"
        ];

        $response = $this->post(route('customers.store'), $data);
        $response->assertStatus(201);
        $response->assertJson($data);
        $response->assertSee($data['first_name']);
        $response->assertSee($data['last_name']);
        $response->assertSee($data['email']);
        $response->assertSee($data['phone']);
        $response->assertSee($data['address']);
    }

    /**
     * Test can list all customers
     */
    public function test_can_list_customers()
    {
        Customer::insert([
            [
                "first_name" => "Jane",
                "last_name" => "Doe",
                "email" => "janedoe@gmail.com",
                "phone" => "256787584128",
                "address" => "kampala Kireka",
                'created_at'    => now(),
                'updated_at'    => now(),
            ],
            [
                "first_name" => "Fred",
                "last_name" => "Oketa",
                "email" => "oketafred@gmail.com",
                "phone" => "256787584128",
                "address" => "kampala Kireka",
                'created_at'    => now(),
                'updated_at'    => now(),
            ]
        ]);

        $response = $this->get(route('customers.index'));
        $response->assertStatus(200);
    }

    /**
     * Test can show a customer
     */
    public function test_can_show_a_customer()
    {
        $customer = Customer::create([
            "first_name" => "Fred",
            "last_name" => "Oketa",
            "email" => "oketafred@gmail.com",
            "phone" => "256787584128",
            "address" => "kampala Kireka"
        ]);

        $response = $this->get(route('customers.show', $customer->id));
        $response->assertStatus(200);
    }

    /**
     * Test can update a customer
     */
    public function test_can_update_a_customer()
    {
        $customer = Customer::create([
            "first_name" => "Fred",
            "last_name" => "Oketa",
            "email" => "oketafred@gmail.com",
            "phone" => "256787584128",
            "address" => "kampala Kireka"
        ]);

        $data = [
            "first_name" => "Fred Updated",
            "last_name" => "Oketa Updated",
            "email" => "oketafredupdated@gmail.com",
            "phone" => "256787584128",
            "address" => "kampala Kireka"
        ];

        $response = $this->patch(route('customers.update', $customer->id), $data);
        $response->assertJson($data);
        $response->assertStatus(202);
    }

    /**
     * Test can delete a customer
     */
    public function test_can_delete_a_customer()
    {
        $customer = Customer::create([
            "first_name" => "Fred",
            "last_name" => "Oketa",
            "email" => "oketafred@gmail.com",
            "phone" => "256787584128",
            "address" => "kampala Kireka"
        ]);

        $response = $this->delete(route('customers.destroy', $customer->id));
        $response->assertStatus(204);
    }
}

<?php

namespace Tests\Feature;

use App\Product;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ProductTest extends TestCase
{
    use RefreshDatabase;
    /**
     * Test can create a product
     */
    public function test_can_create_product()
    {
        $data = [
            "name" => "product name test 321",
            "price" => 7000,
            "description" => "Product description test",
            "service" => true
        ];

        $response = $this->post(route('products.store'), $data);
        $response->assertStatus(201);
        $response->assertJson($data);
        $response->assertSee($data['name']);
        $response->assertSee($data['price']);
        $response->assertSee($data['description']);
        $response->assertSee($data['service']);
    }
    
    /**
     * Tes can list all products
     */
    public function test_can_list_products()
    {
        Product::create([
            "name" => "product name test 321",
            "price" => 7000,
            "description" => "Product description test",
            "service" => true
        ]);

        $response = $this->get(route('products.index'));
        $response->assertStatus(200);
        // $response->assertJson($response->toArray());
        // $response->assertJsonStructure([
        //         '*' => [ 'id', 'title', 'content' ],
        //     ]);
    }

    /**
     * Test can show a product
     */
    public function test_can_show_product() {

        $product = Product::create([
            "name" => "product name test 321",
            "price" => 7000,
            "description" => "Product description test",
            "service" => true
        ]);

        $response = $this->get(route('products.show', $product->id));
        $response->assertStatus(200);
    }

    /**
     * Test can update a product
     */
    public function test_can_update_product() {

        $product = Product::create([
            "name" => "product name test 321",
            "price" => 7000,
            "description" => "Product description test",
            "service" => true
        ]);

        $data = [
            "name" => "product name test 321 updated!",
            "price" => 10000,
            "description" => "Product description test updated!",
            "service" => false
        ];

        $this->patch(route('products.update', $product->id), $data)
            ->assertStatus(202)
            ->assertJson($data);
    }

    /**
     * Test can delete a product
     */
    public function test_can_delete_product() {

        $product = Product::create([
            "name" => "product name test 321",
            "price" => 7000,
            "description" => "Product description test",
            "service" => true
        ]);

        $response = $this->delete(route('products.destroy', $product->id));
        $response->assertStatus(204);
    }
}

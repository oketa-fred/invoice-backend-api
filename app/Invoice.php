<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Invoice extends Model
{
    use SoftDeletes;

    protected $primaryKey = 'invoice_no';
    public $incrementing = false;

    /**
     * Set invoice_no as the primary key.
     */
    public function getRouteKeyName()
    {
        return 'invoice_no';
    }

    protected $fillable = [
        "id",
        "date",
        "customer_id",
        "invoice_no"
    ];

    /**
     * An Invouce has many products.
     */
    public function products()
    {
        return $this->belongsToMany(Product::class, 'invoice_products', 'invoice_no')->withTrashed();
    }

    /**
     * An Invoice belongs to a customer.
     */
    public function customer()
    {
        return $this->belongsTo(Customer::class)->withTrashed();
    }

    public static function boot()
    {
        parent::boot();

        static::creating(function($model) {
            $model->id = Invoice::count() + 1;
        });
    }
}

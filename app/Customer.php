<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Customer extends Model
{
    use SoftDeletes;

    protected $fillable = [
        "first_name",
        "last_name",
        "email",
        "phone",
        "address",
    ];

    /**
     * A Customer has many invoices.
     */
    public function invoices()
    {
        return $this->hasMany(Invoice::class);
    }

    /**
     * Get Customer Full Name.
     */
    public function getCustomerNameAttribute()
    {
        return "{$this->first_name} {$this->last_name}";
    }
}

<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProductRequest;
use App\Http\Resources\ProductCollection;
use App\Http\Resources\ProductResource;
use App\Product;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Symfony\Component\HttpFoundation\Response;

class ProductController extends Controller
{
    /**
    * Display a listing of the products paginated 5 per page 
    *
    * @return \Illuminate\Http\Response
    */
    public function index()
    {
        try {
            $products = Product::paginate(5);
            return new ProductCollection($products);
        } catch (Exception $ex) {
            Log::error("GET_ALL_PRODUCTS: {$ex->getMessage()}");
            return response()->json($ex->getMessage(), Response::HTTP_FORBIDDEN);
        }
    }

    /**
    * Store a newly created product.
    *
    * @param  \Illuminate\Http\ProductRequest  $request
    * @return \Illuminate\Http\Response
    */
    public function store(ProductRequest $request)
    {
        try {
            $product = Product::create($request->all());
            return response()->json(new ProductResource($product), Response::HTTP_CREATED);
        } catch (Exception $ex) {
            Log::error("CREATE_PRODUCT: {$ex->getMessage()}");
            return response()->json($ex->getMessage(), Response::HTTP_FORBIDDEN);
        }
    }

    /**
    * Display a specific product details.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function show($id)
    {
        try {
            $product = Product::whereId($id)->first();
            if (!$product){
                return response()->json(['error' => "Product with id {$id} not found!"]);
            }
            return response()->json(new ProductResource($product));
        } catch (Exception $ex) {
            Log::error("SHOW_A_PRODUCT {$ex->getMessage()}");
            return response()->json($ex->getMessage(), Response::HTTP_FORBIDDEN);
        }
    }

    /**
     * Update a specific product.
     *
     * @param  \Illuminate\Http\ProductRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProductRequest $request, $id)
    {
        try {
            $product = Product::whereId($id)->first();
            if (!$product){
                return response()->json(['error' => "Product with id {$id} not found!"]);
            }
            $product->update($request->all());
            return response()->json(new ProductResource($product), Response::HTTP_ACCEPTED);
        } catch (Exception $ex) {
            Log::error("UPDATE_A_PRODUCT {$ex->getMessage()}");
            return response()->json($ex->getMessage(), Response::HTTP_FORBIDDEN);
        }
    }

    /**
     * Delete a speciic product.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $product = Product::whereId($id)->first();
            if (!$product){
                return response()->json(['error' => "Product with id {$id} not found!"]);
            }
            Product::destroy($product->id);
            return response()->json(null, Response::HTTP_NO_CONTENT);
        } catch (Exception $ex) {
            Log::error("DELETE_A_PRODUCT {$ex->getMessage()}");
            return response()->json($ex->getMessage(), Response::HTTP_FORBIDDEN);
        }
    }
}
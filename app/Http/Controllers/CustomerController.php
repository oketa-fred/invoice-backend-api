<?php

namespace App\Http\Controllers;

use App\Customer;
use App\Http\Requests\CustomerRequest;
use App\Http\Resources\CustomerCollection;
use App\Http\Resources\CustomerResource;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Symfony\Component\HttpFoundation\Response;

class CustomerController extends Controller
{
    /**
     * Display a listing of the customers.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $customers = Customer::paginate(5);
            return new CustomerCollection($customers);
        } catch (Exception $ex) {
            Log::error("GET_ALL_CUSTOMERS {$ex->getMessage()}");
            return response()->json($ex->getMessage(), Response::HTTP_FORBIDDEN);
        }
    }

    /**
    * Store a newly created customer.
    *
    * @param  \Illuminate\Http\CustomerRequest  $request
    * @return \Illuminate\Http\Response
    */
    public function store(CustomerRequest $request)
    {
        try {
            $customer = Customer::create($request->all());
            return response()->json(new CustomerResource($customer), Response::HTTP_CREATED);
        } catch (Exception $ex) {
            Log::error("STORE_A_CUSTOMER {$ex->getMessage()}");
            return response()->json($ex->getMessage(), Response::HTTP_FORBIDDEN);
        }
    }

    /**
    * Display a specific customer details.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function show($id)
    {
        try {
            $customer = Customer::whereId($id)->first();
            if(!$customer) {
                return response()->json(["error" => "Customer with Id {$id} not found!"]);
            }
            return response()->json(new CustomerResource($customer));
        } catch (Exception $ex) {
            Log::error("SHOW_A_CUSTOMER {$ex->getMessage()}");
            return response()->json($ex->getMessage(), Response::HTTP_FORBIDDEN);
        }
    }

    /**
     * Update a specific customer.
     *
     * @param  \Illuminate\Http\CustomerRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CustomerRequest $request, $id)
    {
        try {
            $customer = Customer::find($id);
            if(!$customer) {
                return response()->json(["error" => "Customer with Id {$id} not found!"]);
            }
            $customer->update($request->all());
            return response()->json(new CustomerResource($customer), Response::HTTP_ACCEPTED);
        } catch (Exception $ex) {
            Log::error("UPDATE_A_CUSTOMER {$ex->getMessage()}");
            return response()->json($ex->getMessage(), Response::HTTP_FORBIDDEN);
        }
    }

    /**
     * Delete a speciic customer.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $customer = Customer::whereId($id)->first();
            if (!$customer){
                return response()->json(['error' => "Customer with id {$id} not found!"]);
            }
            Customer::destroy($customer->id);
            return response()->json(null, Response::HTTP_NO_CONTENT);
        } catch (Exception $ex) {
            Log::error("DELETE_A_CUSTOMER {$ex->getMessage()}");
            return response()->json($ex->getMessage(), Response::HTTP_FORBIDDEN);
        }
    }
}

<?php

namespace App\Http\Controllers;

use App\Customer;
use App\Http\Requests\InvoiceRequest;
use App\Http\Resources\InvoiceCollection;
use App\Http\Resources\InvoiceResource;
use App\Invoice;
use App\Product;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Symfony\Component\HttpFoundation\Response;

class InvoiceController extends Controller
{
    /**
    * Display a list of invoices Display a specific customer details.
    *
    * @param  int  $customer_id
    * @return \Illuminate\Http\Response
    */
    public function index($customer_id)
    {
        try {
            $customer = Customer::whereId($customer_id)->first();
            if(!$customer) {
                return response()->json(['error' => "Customer with id {$customer_id} not found"]);
            }
            return new InvoiceCollection($customer->invoices()->paginate(3));
        } catch (Exception $ex) {
            Log::error("GET_ALL_INVOICES {$ex->getMessage()}");
            return response()->json($ex->getMessage(), Response::HTTP_FORBIDDEN);
        }
    }

    /**
    * Display a specific invoice details for a specific customer.
    *
    * @param  int  $customer_id
    * @param  int  $invoice_no
    * @return \Illuminate\Http\Response
    */
    public function show($customer_id, $invoice_no)
    {
        try {
            $customer = Customer::whereId($customer_id)->first();
            if(!$customer) {
                return response()->json(['error' => "Customer with id {$customer_id} not found"]);
            }
            $invoice = Invoice::whereInvoiceNo($invoice_no)->first();
            if(!$invoice) {
                return response()->json(['error' => "Invoice with id {$invoice_no} not found"]);
            }
            return response()->json(new InvoiceResource($invoice));
        } catch (Exception $ex) {
            Log::error("SHOW_AN_INVOICES {$ex->getMessage()}");
            return response()->json($ex->getMessage(), Response::HTTP_FORBIDDEN);
        }
    }
    
    /**
    * Store a newly created invoice for a specific customer.
    *
    * @param  \Illuminate\Http\InvoiceRequest  $request
    * @param  int  $customer_id
    * @return \Illuminate\Http\Response
    */
    public function store(InvoiceRequest $request, $customer_id)
    {
        try {
            $customer = Customer::whereId($customer_id)->first();
            if (!$customer){
                return response()->json(['error' => "Customer with id {$customer_id} not found!"]);
            }
            $request["invoice_no"] = $this->invoiceNumberGenerator();
            $request["customer_id"] = $customer_id;
            $invoice = Invoice::create($request->all());
            if (array_diff($request->input('products'), Product::pluck('id')->toArray())) {
                return response()->json(['error' => "Please select only available product ids"]);
            }
            foreach($request->input('products') as $product_id) {
                DB::table('invoice_products')->insert([
                    'invoice_no' => $invoice->invoice_no, 
                    'product_id' => $product_id
                ]);
            }
            return response()->json(new InvoiceResource($invoice));
        } catch (Exception $ex) {
            Log::error("STORE_AN_INVOICE {$ex->getMessage()}");
            return response()->json($ex->getMessage(), Response::HTTP_FORBIDDEN);
        }
    }

    /**
    * Update a specific invoice details for a specific customer.
    *
    * @param  \Illuminate\Http\InvoiceRequest  $request
    * @param  int  $customer_id
    * @param  int  $invoice_no
    * @return \Illuminate\Http\Response
    */
    public function update(InvoiceRequest $request, $customer_id, $invoice_no)
    {
        try {
            $customer = Customer::whereId($customer_id)->first();
            if (!$customer){
                return response()->json(['error' => "Customer with Id {$customer_id} not found!"]);
            }
            $invoice = Invoice::whereInvoiceNo($invoice_no)->first();
            if(!$invoice) {
                return response()->json(['error' => "Invoice with invoice_no {$invoice_no} not found"]);
            }
            $invoice->update($request->all());
            if (array_diff($request->input('products'), Product::pluck('id')->toArray())) {
                return response()->json(['error' => "Please select only available product ids"]);
            }
            DB::table('invoice_products')->where('invoice_no', $invoice->invoice_no)->delete();
            foreach($request->input('products') as $product_id) {
                DB::table('invoice_products')->insert([
                    'invoice_no' => $invoice->invoice_no,
                    'product_id' => $product_id
                ]);
            }
            return response()->json(new InvoiceResource($invoice), Response::HTTP_ACCEPTED);
        } catch (Exception $ex) {
            Log::error("UPDATE_AN_INVOICE {$ex->getMessage()}");
            return response()->json($ex->getMessage(), Response::HTTP_FORBIDDEN);
        }
    }

    /**
    * Delete a speciic invoice for a specific customer.
    *
    * @param  int  $customer_id
    * @param  int  $invoice_no
    * @return \Illuminate\Http\Response
    */
    public function destroy($customer_id, $invoice_no)
    {
        try {
            $customer = Customer::whereId($customer_id)->first();
            if (!$customer){
                return response()->json(['error' => "Customer with Id {$customer_id} not found!"]);
            }
            $invoice = Invoice::whereInvoiceNo($invoice_no)->first();
            if (!$invoice){
                return response()->json(['error' => "Invoice with invoice_no {$invoice_no} not found!"]);
            }
            DB::table('invoice_products')->where('invoice_no', $invoice->invoice_no)->delete();
            Invoice::destroy($invoice->invoice_no);
            return response()->json(null, Response::HTTP_NO_CONTENT);
        } catch (Exception $ex) {
            Log::error("DELETE_AN_INVOICE {$ex->getMessage()}");
            return response()->json($ex->getMessage(), Response::HTTP_FORBIDDEN);
        }
    }

    /**
     * Invoice Numbergenerator
     *
     * @return invoice_no
     */
    public function invoiceNumberGenerator()
    {
        return time() . Invoice::count();
    }
}

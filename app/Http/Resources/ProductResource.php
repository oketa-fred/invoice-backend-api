<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id"            => $this->id,
            "name"          => $this->name,
            "price"         => (integer) $this->price,
            "description"   => $this->description,
            "service"       => !!$this->service,
            'createdAt'     => $this->created_at->format('Y-m-d H:i:s'),
        ];
    }
}

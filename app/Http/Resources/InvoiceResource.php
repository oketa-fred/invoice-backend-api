<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class InvoiceResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "invoice_no" => $this->invoice_no,
            "date" => $this->date,
            "customer_name" => $this->customer->customer_name,
            'createdAt' => $this->created_at->format('Y-m-d H:i:s'),
            "products" => ProductResource::collection($this->products),
        ];
    }
}

<?php

use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    return response()->json([
        'message' => 'Invoice Backend API - Future Link Technologies',
        'developer' => 'Oketa Fred',
        'email' => 'oketafred@gmail.com',
        'telephoneNumber' => '+256 787-584-128',
        'documentationUrl' => 'https://documenter.getpostman.com/view/6266273/TW74jR5k'
    ]);
});
